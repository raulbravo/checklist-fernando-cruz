import { ControlService } from './../services/control.service';
import { Category } from './../interfaces/category';
import { ApiService } from './../services/api.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css'],
})
export class CategoriesComponent implements OnInit {
  formNewCategory = new FormGroup({
    newCategory: new FormControl(''),
  });

  formEditCategory = new FormGroup({
    newNameCategory: new FormControl(''),
  });

  categoryEdit: Category = {};

  constructor(
    public apiService: ApiService,
    public controlService: ControlService
  ) {}

  async ngOnInit() {
    try {
      await this.apiService.getCategories();
    } catch (error) {
      console.error('Error: getCategories(): ' + error);
    }
  }

  openEditCategories(category: Category) {
    this.controlService.displayEditCategory = true;

    this.formEditCategory.controls['newNameCategory'].setValue(category.name);
    this.categoryEdit.id = category.id;
  }

  async addCategory() {
    this.apiService.categories.push(
      await this.apiService.addCategory(this.formNewCategory.value.newCategory)
    );

    this.formNewCategory.reset();
  }

  async editCategory() {
    this.categoryEdit.name = this.formEditCategory.value.newNameCategory;

    try {
      await this.apiService.editCategory(this.categoryEdit);
      await this.apiService.getCategories();
      this.controlService.displayEditCategory = false;
    } catch (error) {
      console.error('Error: editCategory(): ' + error);
    }
  }

  async deleteCategory(id: string) {
    try {
      await this.apiService.deleteCategory(id);
      this.apiService.categories.splice(
        this.apiService.categories.map((category) => category.id).indexOf(id),
        1
      );
    } catch (error) {
      console.error(error);
    }
  }

  buttonCancel() {
    this.controlService.displayEditCategory = false;
  }
}
