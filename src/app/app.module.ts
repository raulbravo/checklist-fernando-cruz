import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CategoriesComponent } from './categories/categories.component';
import { NewTaskComponent } from './new-task/new-task.component';

import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { TreeModule } from 'primeng/tree';
import { ButtonModule } from 'primeng/button';
import { ChipsModule } from 'primeng/chips';
import { HttpClientModule } from '@angular/common/http';
import { FilterPipe } from './pipes/filter.pipe';
import { TableModule } from 'primeng/table';
import { TableComponent } from './table/table.component';
import { TagModule } from 'primeng/tag';
import { SubtaskComponent } from './subtask/subtask.component';
import { InputNumberModule } from 'primeng/inputnumber';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    NewTaskComponent,
    FilterPipe,
    TableComponent,
    SubtaskComponent,
  ],
  imports: [
    BrowserModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    DialogModule,
    CheckboxModule,
    TreeModule,
    ButtonModule,
    ChipsModule,
    HttpClientModule,
    TableModule,
    TagModule,
    InputNumberModule,
    TooltipModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
