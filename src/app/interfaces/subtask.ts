export interface Subtask {
  id?: String;
  text?: String;
  done?: boolean;
  sortNumber?: Number;
}
