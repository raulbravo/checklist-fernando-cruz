import { Subtask } from './subtask';
import { Tag } from './tag';
import { Category } from './category';

export interface Task {
  id?: String;
  text?: String;
  category?: Category | null;
  tags?: Tag[];
  subTasks?: Subtask[];
  done?: boolean;
  sortNumber?: Number;
}
