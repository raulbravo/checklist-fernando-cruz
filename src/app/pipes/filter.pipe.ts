import { Task } from './../interfaces/task';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(tasks: Task[], ...args: any): Task[] {
    const searchText = args[0].toLowerCase();
    const searchCategory = args[1];
    const searchTag = args[2];
    const searchShowDoneTask = args[3];

    return tasks
      .filter((task) => task.text?.toLowerCase().includes(searchText))
      .filter((task) => {
        if (task.category != null) {
          return task.category?.id?.includes(searchCategory.id);
        } else return true;
      })
      .filter(
        (task) =>
          !searchTag.name ||
          task.tags?.find((tag) => tag.id?.includes(searchTag.id))
      )
      .filter((task) => {
        if (searchShowDoneTask) return task;
        else return task.done == searchShowDoneTask;
      });
  }
}
