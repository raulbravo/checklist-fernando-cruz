import { ControlService } from './../services/control.service';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Tag } from '../interfaces/tag';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css'],
})
export class NewTaskComponent implements OnInit {
  formNewTask = new FormGroup({
    text: new FormControl('', Validators.required),
    category: new FormControl(''),
    arrayTags: new FormControl(''),
  });

  constructor(
    public apiService: ApiService,
    public controlService: ControlService
  ) {}

  async ngOnInit() {
    try {
      await this.apiService.getCategories();
    } catch (error) {
      console.error('Error: getCategories(): ' + error);
    }

    if (this.controlService.displayEditTask) {
      this.chargeTask();
    }
  }

  chargeTask() {
    this.formNewTask.controls['text'].setValue(
      this.controlService.taskToEdit.text
    );
    this.formNewTask.controls['category'].setValue(
      this.controlService.taskToEdit.category
    );
    this.formNewTask.controls['arrayTags'].setValue(
      this.controlService.taskToEdit.tags?.map((tag) => tag.name)
    );
  }

  async buttonNewTask() {
    let tagsTask: string[] = [];
    let categoryId: string = '';
    if (this.formNewTask.valid) {
      try {
        if (this.controlService.displayNewTask) {
          if (this.formNewTask.value.arrayTags !== '') {
            tagsTask = await this.createArrayTagsId(
              this.formNewTask.value.arrayTags
            );
          }
          if (this.formNewTask.value.category.id != undefined) {
            categoryId = this.formNewTask.value.category.id;
          }
          this.apiService.tasks.push(
            await this.apiService.addTask(
              this.formNewTask.value.text,
              categoryId,
              tagsTask
            )
          );
          await this.apiService.getTasks();
          this.controlService.displayNewTask = false;
        } else if (this.controlService.displayEditTask) {
          this.controlService.taskToEdit.text = this.formNewTask.value.text;
          this.controlService.taskToEdit.category =
            this.formNewTask.value.category;
          await this.apiService.editTask(
            this.controlService.taskToEdit,
            await this.createArrayTagsId(this.formNewTask.value.arrayTags)
          );
          await this.apiService.getTasks();
          this.controlService.displayEditTask = false;
        }
      } catch (error) {
        console.error('Error: buttonNewTask(): ' + error);
      }
    } else {
      this.formNewTask.markAsDirty();
    }
  }

  private async createArrayTagsId(arrayTags: string[]) {
    let arrayTagsToAdd: Tag[] = [];

    this.apiService.tags.forEach((tag) => {
      arrayTags.forEach((tagName, index) => {
        if (tag.name?.toLowerCase() == tagName.toLowerCase()) {
          arrayTagsToAdd.push(tag);
          arrayTags.splice(index, 1);
        }
      });
    });

    for (let tagName of arrayTags) {
      const tag = await this.apiService.addTag(tagName);
      this.apiService.tags.push(tag);
      arrayTagsToAdd.push(tag);
    }
    return arrayTagsToAdd.map((tag) => tag.id || '');
  }

  buttonCancel() {
    this.controlService.displayNewTask = false;
    this.controlService.displayEditTask = false;
  }
}
