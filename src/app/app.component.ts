import { ControlService } from './services/control.service';
import { Component } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'CheckList';

  constructor(
    public apiService: ApiService,
    public controlService: ControlService
  ) {}

  async ngOnInit() {
    try {
      await this.apiService.getCategories();
    } catch (error) {
      console.error('Error: getCategories(): ' + error);
    }

    try {
      await this.apiService.getTags();
    } catch (error) {
      console.error('Error: getTags(): ' + error);
    }

    try {
      await this.controlService.countTasks();
    } catch (error) {
      console.error('Error: countTasks(): ' + error);
    }
  }

  buttonClearFilter() {
    this.controlService.searchText = '';
    this.controlService.selectedCategory = {
      id: '',
      name: '',
    };
    this.controlService.selectedTag = {
      id: '',
      name: '',
    };
    this.controlService.showDoneTask = false;
  }

  openCategories() {
    this.controlService.displayCategories = true;
  }
  openNewTask() {
    this.controlService.displayNewTask = true;
  }
  closeNewTask() {
    this.controlService.buttonCancelModal = false;
  }
  closeEditTask() {
    this.controlService.buttonCancelModal = false;
  }
}
