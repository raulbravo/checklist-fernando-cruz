import { Subtask } from './../interfaces/subtask';
import { ControlService } from './../services/control.service';
import { ApiService } from './../services/api.service';
import { Component, OnInit } from '@angular/core';
import { Task } from '../interfaces/task';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  constructor(
    public apiService: ApiService,
    public controlService: ControlService
  ) {}

  async ngOnInit() {
    try {
      await this.apiService.getTasks();
    } catch (error) {
      console.error('Error: getTasks(): ' + error);
    }
  }

  newSubTask(idTask: string) {
    this.controlService.newSubTask(idTask);
    this.controlService.displayNewSubTask = true;
  }

  editTask(task: Task) {
    this.controlService.editTask(task);
    this.controlService.displayEditTask = true;
  }

  async editSubTask(idTask: string, subTask: Subtask) {
    this.controlService.editSubTask(subTask, idTask);
    this.controlService.displayEditSubTask = true;
  }

  async deleteTask(id: string) {
    try {
      await this.apiService.deleteTask(id);
      this.apiService.tasks.splice(
        this.apiService.tasks.map((task) => task.id).indexOf(id),
        1
      );
      await this.apiService.getTasks();
    } catch (error) {
      console.error('Error: deleteTask(): ' + error);
    }
  }

  async deleteSubTask(idTask: string, idSubTask: string) {
    try {
      await this.apiService.deleteSubTask(idTask, idSubTask);
      this.apiService.tasks.forEach((task) => {
        task.subTasks?.forEach((subTask, i) => {
          if (idSubTask === subTask.id) {
            task.subTasks?.splice(i, 1);
          }
        });
      });
    } catch (error) {
      console.error('Error: deleteSubTask(): ' + error);
    }
  }

  async setDoneTask(task: Task) {
    try {
      await this.apiService.setTaskDone(task);
      await this.apiService.getTasks();
      await this.controlService.countTasks();
    } catch (error) {
      console.error('Error: setDoneTask(): ' + error);
    }
  }

  async setDoneSubTask(idTask: string, subTask: Subtask) {
    try {
      await this.apiService.setSubTaskDone(idTask, subTask);
      await this.apiService.getTasks();
    } catch (error) {
      console.error('Error: setDoneSubTask(): ' + error);
    }
  }

  closeModalSubTask() {
    this.controlService.buttonCancelModal = false;
  }
}
