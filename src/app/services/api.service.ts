import { Subtask } from './../interfaces/subtask';
import { Tag } from './../interfaces/tag';
import { Task } from './../interfaces/task';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Category } from '../interfaces/category';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private url = 'https://checklistbackend.azurewebsites.net/api/';

  public categories: Category[] = [];
  public tasks: Task[] = [];
  public tags: Tag[] = [];

  constructor(private http: HttpClient) {}

  public async getCategories() {
    this.categories = await this.http
      .get<Category[]>(this.url + 'categories')
      .toPromise();
  }

  public async getTags() {
    this.tags = await this.http.get<Tag[]>(this.url + 'tags').toPromise();
  }

  public async addTag(nameTag: string): Promise<Tag> {
    return await this.http
      .post<Tag>(this.url + 'tags', { name: nameTag })
      .toPromise();
  }

  public async getTasks(): Promise<Task[]> {
    return (this.tasks = await this.http
      .get<Task[]>(this.url + 'tasks')
      .toPromise());
  }

  public async addCategory(nameCategory: string): Promise<Category> {
    return await this.http
      .post<Category>(this.url + 'categories', {
        name: nameCategory,
      })
      .toPromise();
  }

  public async editCategory(category: Category): Promise<any> {
    return await this.http
      .patch(this.url + 'categories/' + category.id, [
        {
          op: 'replace',
          path: 'name',
          value: category.name,
        },
      ])
      .toPromise();
  }

  public async deleteCategory(idCategory: string): Promise<any> {
    return await this.http
      .delete(this.url + 'categories/' + idCategory)
      .toPromise();
  }

  public async addTask(
    textTask: string,
    idCategory: string,
    arrayIdTags: string[]
  ): Promise<Task> {
    return await this.http
      .post<Task>(this.url + 'tasks', {
        text: textTask,
        sortNumber: 0,
        categoryId: idCategory,
        tags: arrayIdTags,
      })
      .toPromise();
  }

  public async editTask(taskEdit: Task, arrayIdTags: string[]) {
    return await this.http
      .patch(this.url + 'tasks/' + taskEdit.id, [
        {
          op: 'replace',
          path: 'text',
          value: taskEdit.text,
        },
        {
          op: 'replace',
          path: 'categoryId',
          value: taskEdit.category?.id,
        },
        {
          op: 'replace',
          path: 'tags',
          value: arrayIdTags,
        },
      ])
      .toPromise();
  }

  public async newSubTask(
    idTask: string,
    text: string,
    sortNumber: number
  ): Promise<Subtask> {
    return await this.http
      .post<Subtask>(this.url + 'tasks/' + idTask + '/subtasks', {
        text: text,
        sortNumber: sortNumber,
      })
      .toPromise();
  }

  public async editSubTask(
    idTask: string,
    subTask: Subtask,
    text: string,
    sortNumber: number
  ) {
    return await this.http
      .patch(this.url + 'tasks/' + idTask + '/subtasks/' + subTask.id, [
        {
          op: 'replace',
          path: 'text',
          value: text,
        },
        {
          op: 'replace',
          path: 'sortNumber',
          value: sortNumber,
        },
      ])
      .toPromise();
  }

  public async setTaskDone(taskEdit: Task) {
    return await this.http
      .patch(this.url + 'tasks/' + taskEdit.id, [
        {
          op: 'replace',
          path: 'done',
          value: taskEdit.done,
        },
      ])
      .toPromise();
  }

  public async setSubTaskDone(idTask: string, subTask: Task) {
    return await this.http
      .patch(this.url + 'tasks/' + idTask + '/subtasks/' + subTask.id, [
        {
          op: 'replace',
          path: 'done',
          value: subTask.done,
        },
      ])
      .toPromise();
  }

  public async deleteTask(idTask: string): Promise<any> {
    return await this.http.delete(this.url + 'tasks/' + idTask).toPromise();
  }

  public async deleteSubTask(idTask: string, idSubTask: string): Promise<any> {
    return await this.http
      .delete(this.url + 'tasks/' + idTask + '/subtasks/' + idSubTask)
      .toPromise();
  }
}
