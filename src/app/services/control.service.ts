import { Subtask } from './../interfaces/subtask';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import { Category } from '../interfaces/category';
import { Tag } from '../interfaces/tag';
import { Task } from '../interfaces/task';

@Injectable({
  providedIn: 'root',
})
export class ControlService {
  displayNewTask: boolean = false;
  displayEditTask: boolean = false;
  buttonCancelModal: boolean = false;
  displayCategories: boolean = false;
  displayEditCategory: boolean = false;
  displayEditSubTask: boolean = false;
  displayNewSubTask: boolean = false;

  taskToEdit: Task = {};
  subTaskToEdit: Subtask = {};

  searchText: string = '';

  selectedCategory: Category = {
    id: '',
    name: '',
  };
  selectedTag: Tag = {
    id: '',
    name: '',
  };

  tasksToDo: number = 0;
  tasksDone: number = 0;

  showDoneTask: boolean = false;
  taskDone: boolean = false;

  idTask: string = '';

  constructor(private apiService: ApiService) {}

  editTask(task: Task) {
    this.taskToEdit = task;
  }

  newSubTask(id: string) {
    this.idTask = id;
  }

  editSubTask(subTask: Task, id: string) {
    this.subTaskToEdit = subTask;
    this.idTask = id;
  }

  async countTasks() {
    this.tasksDone = 0;
    this.tasksToDo = 0;
    try {
      const tasks = await this.apiService.getTasks();
      tasks.map((task) => {
        if (task.done) this.tasksDone++;
        else this.tasksToDo++;
      });
    } catch (error) {
      console.error('Error: getTasks(): ' + error);
    }
  }
}
