import { ApiService } from './../services/api.service';
import { ControlService } from './../services/control.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-subtask',
  templateUrl: './subtask.component.html',
  styleUrls: ['./subtask.component.css'],
})
export class SubtaskComponent implements OnInit {
  formSubTask = new FormGroup({
    text: new FormControl('', Validators.required),
    sortNumber: new FormControl(''),
  });

  constructor(
    private controlService: ControlService,
    private apiService: ApiService
  ) {}

  ngOnInit(): void {
    if (this.controlService.displayEditSubTask) {
      this.chargeSubTask();
    }
  }

  async buttonNewSubTask() {
    if (this.formSubTask.valid) {
      try {
        if (this.controlService.displayNewSubTask) {
          await this.apiService.newSubTask(
            this.controlService.idTask,
            this.formSubTask.value.text,
            this.formSubTask.value.sortNumber
          );
          await this.apiService.getTasks();
          this.controlService.displayNewSubTask = false;
        } else if (this.controlService.displayEditSubTask) {
          await this.apiService.editSubTask(
            this.controlService.idTask,
            this.controlService.subTaskToEdit,
            this.formSubTask.value.text,
            this.formSubTask.value.sortNumber
          );
          await this.apiService.getTasks();
          this.controlService.displayEditSubTask = false;
        }
      } catch (error) {
        console.error('Error: buttonNewSubTask(): ' + error);
      }
    } else {
      this.formSubTask.markAsDirty();
    }
  }

  chargeSubTask() {
    this.formSubTask.controls['text'].setValue(
      this.controlService.subTaskToEdit.text
    );
    this.formSubTask.controls['sortNumber'].setValue(
      this.controlService.subTaskToEdit.sortNumber
    );
  }

  buttonCancel() {
    this.controlService.displayEditSubTask = false;
    this.controlService.displayNewSubTask = false;
  }
}
